Gait-Tracking-With-x-IMU
========================

This is a fork from the original project by S. Madgwick. It is intended to translate the Gait tracking algorithm to python, in order to adapt it to a real time motion capture processing in the context of the [Chordata framework](http://chordata.cc).


The algorithm translation is done in the jupyter notebook `Gait Tracking With x-IMU/gait_tracking_wit_imu.ipynb`

In order to view and edit the notebook you should have [jupyter](https://jupyter.org/) installed. The easiest way to do it, while gaining access to the data manipulation tools used in this development is installing [anaconda](https://www.anaconda.com/distribution/).

The notebook environment is really handy for this type of task since it allows to mix notes in markdown with code and plots of the data.

![screenshot jupyter](Screenshot_Jupyter.png)

In this notebook each section of the original `matlab` code is comented, and should be replicated step by step in python.

## Using `octave` to run the scripts

You might find a couple of errors if trying to run the original `Script.m` with `octave`.

- If you get:
```matlab
Error using xIMUdataClass (line 49)
No data was imported.
```

see this issue:
https://github.com/xioTechnologies/Gait-Tracking-With-x-IMU/issues/6 

- If the error is:
```markdown
warning: the 'butter' function belongs to the signal package from Octave Forge
which seems to not be installed in your system.
```

then instaling the signal package

```bash
sudo apt install octave-signal 
```
and loading it in octave

```octave
pkg load signal
```

should fix it


## Other translations to python:

This seems like a good example, probably not finished, but many of the steps already done.

https://github.com/italogsfernandes/Gait-Tracking-With-x-IMU/tree/master/Python%20Gait%20Tracking


## Original README below: 

This is the source code for the foot tracking algorithm demonstrated in Seb Madgwick's "[3D Tracking with IMU"](http://www.youtube.com/watch?v=6ijArKE8vKU) video, originally uploaded to YouTube in March 2011.  An [x-IMU](http://www.x-io.co.uk/x-imu) attached to a foot is be used to track position through [dead reckoning](http://en.wikipedia.org/wiki/Dead_reckoning) and integral drift corrected for each time the foot hit the ground.

See the [original post](http://www.x-io.co.uk/gait-tracking-with-x-imu/) for more information.

========================


![x-IMU attached to foot](Screenshot%20-%20x-IMU%20Attached%20To%20Foot.png)
![x-IMU attached to foot](Screenshot%20-%20MATLAB%20Animation%20Close-Up.png)
![x-IMU attached to foot](Screenshot%20-%20MATLAB%20Animation%20Spiral%20Stairs.png)
